package com.springboot.parkinglot.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
public class VehicleResponse {

    @JsonProperty
    private String ownerName;
}
