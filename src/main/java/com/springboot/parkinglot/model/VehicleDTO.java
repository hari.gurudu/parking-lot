package com.springboot.parkinglot.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VehicleDTO {

    @Id
    @NonNull
    private Long id;

    private String ownerName;
    private String vehicleNumber;

}
