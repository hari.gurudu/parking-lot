package com.springboot.parkinglot.serviceImpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.springboot.parkinglot.model.VehicleDTO;
import com.springboot.parkinglot.repository.ParkingRepository;
import com.springboot.parkinglot.service.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ParkingServiceImpl implements ParkingService {

    @Autowired
    private ParkingRepository parkingRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public List<VehicleDTO> getAllVehicles() {
        return parkingRepository.findAll();
    }

    @Override
    public String park(VehicleDTO vehicleDTO) {
        if(vehicleDTO.getId() != null){
            parkingRepository.save(vehicleDTO); // post
            return "Parked";
        }
        return "not Parked";
    }

    @Override
    public String unPark(Long id) {
        List<VehicleDTO> dtos = parkingRepository.findAll();
        if (id > 0 && id <= 10 && dtos.stream().anyMatch(i -> i.getId() == id)) {
            parkingRepository.deleteById(id);
        } else {
            throw new RuntimeException("Kindly provide id between 1 and 10");
        }
        return "Unparked vehicle at slot number" + id;
    }

    @Override
    public String parkall(List<VehicleDTO> vehicleDTOList) {
  // LinkedHashMap<String, Object>
//        List<VehicleDTO> list = vehicleDTOList.stream().map(obj -> VehicleDTO.builder().id(Long.valueOf(obj.get("id").toString())).vehicleNumber((String) obj.get("vehicleNumber"))
//                .ownerName((String) obj.get("ownerName")).build()).
//                 collect(Collectors.toList());

//        JavaType type = objectMapper.getTypeFactory().
//                constructCollectionType(List.class, VehicleDTO.class);
//        try {
//            list = objectMapper.readValue( objectMapper.writeValueAsString(vehicleDTOList), type);
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
        parkingRepository.saveAll(vehicleDTOList);
        return  "parked all";
    }


}

