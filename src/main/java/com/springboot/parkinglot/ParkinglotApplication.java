package com.springboot.parkinglot;

import ch.qos.logback.core.net.SyslogOutputStream;
import org.apache.commons.validator.routines.DateValidator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@SpringBootApplication
public class ParkinglotApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParkinglotApplication.class, args);
	    System.out.println("hallo .....123");

		//System.out.println(new BigDecimal("155.5666"));
		//System.out.println(new BigDecimal("jk55"));
		String limit = null;
       //System.out.println("big decimal.."+ Long.valueOf(limit));
		Map<String, String> linkedMap = new LinkedHashMap<>();
		//linkedMap.put("Bangalore", "Hyderabad");
		linkedMap.put("Bangalore", "Hyde");
		linkedMap.put("mumbai", "delhi");
		linkedMap.put("delhi", "chennai");
		linkedMap.put("chennai", "Bangalore");

		linkedMap.remove("Bangalore");
		linkedMap.put("Bangalore", "Hyderabad");
		for (Map.Entry<String, String> entry : linkedMap.entrySet()) {
			System.out.println(entry.getKey() + " = " + entry.getValue());
		}

		boolean isValidDate = dateValidator("2021-13-25", "yyyy-mm-dd");
		System.out.println("is valid date..."+ isValidDate);
	}

	public static boolean dateValidator(String date, String format)
	{
		// Get the `DateValidator` instance
		DateValidator validator = DateValidator.getInstance();
		// Validate the date
		return validator.isValid(date, format);
	}
}
