package com.springboot.parkinglot.service;

import com.springboot.parkinglot.model.VehicleDTO;

import java.util.LinkedHashMap;
import java.util.List;

public interface ParkingService {

    List<VehicleDTO> getAllVehicles();
    String park(VehicleDTO vehicleDTO); // save i.e insert new vehicle
    String unPark(Long id); // delete i.e delete an existing vehicle with given id
    String parkall(List<VehicleDTO> vehicleDTOList);
}
