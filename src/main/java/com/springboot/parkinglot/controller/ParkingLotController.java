package com.springboot.parkinglot.controller;


import com.springboot.parkinglot.model.VehicleDTO;
import com.springboot.parkinglot.model.VehicleResponse;
import com.springboot.parkinglot.service.ParkingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Controller
@RestController
public class ParkingLotController {
    @Autowired
    ParkingService parkingService;

//    @GetMapping("/aboutMe")
//   // @ResponseBody
//    public Employee aboutMe(){
//        return Employee.builder().name("Hari").age(31).skills("Fullstack web developer").build();
//    }
//
//    @PostMapping("/infoAboutEmployee")
//    public Employee infoAboutEmployee(String name, int age, String skills){
//        return Employee.builder().name(name).age(age).skills(skills).build();
//    }
    @GetMapping("/")
    public String getIndexPage(){
        return "Hi, How are you ?";
    }

     @GetMapping("/parkingInfo")
     public ResponseEntity<Object> getAllVehicles(){
          List<VehicleDTO> vehicles = parkingService.getAllVehicles();
          return ResponseEntity.ok(vehicles);
     }

     @PostMapping("/park")
    public String park(@RequestBody VehicleDTO vehicleDTO){
         System.out.println(vehicleDTO);
         return parkingService.park(vehicleDTO);
     }


    @PostMapping("/parkall")
    public ResponseEntity<Object> parkall(@RequestBody List<VehicleDTO> vehicleDTOList){
        System.out.println(vehicleDTOList);
      // parkingService.parkall((List<LinkedHashMap<String, Object>>) vehicleDTOList.get("items"));
          parkingService.parkall(vehicleDTOList);
        return ResponseEntity.created(null).build();
        //return "parked all";
    }

    // check for sending http status code( 201 if saved or other code if it is not possible to save(duplicate id's) or has null values)

     @DeleteMapping("/unpark")
    public String unpark(Long id){
         return parkingService.unPark(id);
     }

}



